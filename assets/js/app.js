//Width and height
// set the dimensions and margins of the graph
var margin = {top: 10, right: 30, bottom: 30, left: 60},
    width = 500 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;
var svgHeight = 500;
var svgWidth = 960;

// append the svg object to the body of the page
var svg = d3.select("#scatter")
  .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");


//Read the data
d3.csv("assets/data/data.csv")
.then(function(data) {

  var x = d3.scaleLinear()
    .domain([0, d3.max(data, function(d){return d.poverty *.8})])
    .range([ 0, width ]);
  svg.append("g")
  .attr("transform", "translate(0," + height + ")")
  .call(d3.axisBottom(x));
    
  svg.append("text")             
      .attr("transform",
            "translate(" + (width/2) + " ," + 
                           (height + margin.top + 18) + ")")
      .style("text-anchor", "middle")
      .text("Poverty");

    // text label for the y axis
    svg.append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 0 - margin.left)
    .attr("x",0 - (height / 2))
    .attr("dy", "1em")
    .style("text-anchor", "middle")
    .text("Healthcare");  


  var y = d3.scaleLinear()
  .domain([0, d3.max(data, function(d){return d.healthcare*1.2})])
  .range([ height, 0]);
  svg.append("g")
  .call(d3.axisLeft(y));

  // Add dots
  svg.append('g')
    .selectAll("dot")
    .data(data)
    .enter()
    .append("circle")
    .attr("cx", function (d) {return x(d.poverty * .6); } )
    .attr("cy", function (d) {return y(d.healthcare ); } )
    .attr("r", 7)
    .style("fill", "green")
    //I couldn't figure out how to get the State lables to show :(
    // these lines of code are placing the text there but can't get it to display
    .append("text")
    .text(function(d) {
      console.log(d.state.slice(0,2));
      return d.state.slice(0,2);
    })
    .style("fill", "black" )
    .sytle("z-index", "-1");

});